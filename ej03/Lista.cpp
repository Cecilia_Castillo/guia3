#include <iostream>
#include <string>
using namespace std;
#include "Lista.h"


Lista::Lista() {}

void Lista::crear (int numero) {
	Nodo *tmp;

	/* Se crea el nuevo nodo */
	tmp = new Nodo;

	/* se asigna una instancia al numero */
	tmp->numero = numero;
    
	/* apunta a NULL por defecto.  */
	tmp->sig = NULL;

	/* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
	if (this->raiz == NULL) {
		this->raiz = tmp;

	} else {
		//variables auxiliares para buscar posicion
		Nodo *aux_init, *aux_;
		
		aux_init = this->raiz; 
		
		//para recorrer la lista
		while((aux_init != NULL) && (aux_init->numero < numero)){
			aux_ = aux_init;
			aux_init = aux_init->sig;
		}
		
		// se revisa si debe ir al principio de la lista
		if(aux_init == this->raiz){
			this->raiz = tmp;
			this->raiz->sig = tmp;
		}else{
			aux_->sig = tmp;
			tmp->sig = aux_init;
		}

	}
}

void Lista::imprimir () {
	/* variable temporal para recorrer la lista */
	Nodo *tmp = this->raiz;

	/* la recorre mientras sea distinto de NULL */
	while (tmp != NULL) {
		cout << tmp->numero << " ";
		tmp = tmp->sig;
	}
}

void Lista::completar_lista(Lista *lista, string letra) {
	int opcion = 0;
	string ingreso;
    
	do{
		cout << "\nEsta es la lista" << letra << endl;
		cout << "1.- Insertar elemento" << endl;
		cout << "2.- Mostrar lista" << endl;
		cout << "3.- Lista completada" << endl;
		cout << "Ingrese opcion" << endl;
		cin >> opcion;
		
		switch (opcion){
			case 1: cout << "Ingrese numero" << endl;
					cin >> ingreso;
					lista->crear(stoi(ingreso));
					break;
			case 2: lista->imprimir();
					break;
			}
		
		}while(opcion !=3);
}

void Lista::mezclar_listas(Lista *mezclada, Lista *original){
	Nodo *tmp;
	
	tmp = original->raiz;
	
	while(tmp != NULL){
		mezclada->crear(tmp->numero);
		tmp = tmp->sig;
	}
	
}

void Lista::rellenar_lista(Lista *completa, Lista *ingresada){
	Nodo *tmp, *aux;
	int numero;
	int aumentador = 0;
	
	tmp = ingresada->raiz;
	aux->numero = numero;
	
	/*se parte desde que tmp es distinto de NULL
	y desde que el numero en la lista es distinto del que deber`ia ser el siguiente */
	while(tmp != NULL){
		if(tmp->numero != numero+1){
		aumentador = numero;
		numero = numero++;
		completa->crear(tmp->numero);
		tmp = tmp->sig;
		}else{
		completa->crear(tmp->numero);
		
		}
	}
	
	
}
