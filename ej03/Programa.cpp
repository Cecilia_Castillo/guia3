#include <string>
using namespace std;
#include "Lista.h"

int main (int argc, char **argv) {
	string a;
	string b;
	//se crea el objeto lista
	Lista *lista_a = new Lista();
	
	lista_a->completar_lista(lista_a, a);
	
	Lista *lista_b = new Lista();
	lista_b->rellenar_lista(lista_b, lista_a);
	lista_b->mezclar_listas(lista_b, lista_a);
	
	lista_a->imprimir();
	lista_b->imprimir();
	
	//libera espacio en memoria
	delete lista_a;
	delete lista_b;

	return 0;
}
