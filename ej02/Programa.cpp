#include <string>
using namespace std;
#include "Lista.h"

int main (int argc, char **argv) {
	string a;
	string b;
	//se crean los objetos lista
	Lista *lista_a = new Lista();
	Lista *lista_b = new Lista();
	
	lista_a->completar_lista(lista_a, a);
	lista_b->completar_lista(lista_b, b);
	
	//lista mezclada
	Lista *lista_c = new Lista();
	
	lista_c->mezclar_listas(lista_c, lista_a);
	lista_c->mezclar_listas(lista_c, lista_b);
	
	lista_a->imprimir();
	lista_b->imprimir();
	lista_c->imprimir();
	
	//libera espacio en memoria
	delete lista_a;
	delete lista_b;
	return 0;
}
