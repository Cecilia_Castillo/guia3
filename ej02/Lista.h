#ifndef LISTA_H
#define LISTA_H

#include <iostream>
#include <string>
using namespace std;

/* define la estructura del nodo. */
typedef struct _Nodo {
	int numero;
	struct _Nodo *sig;
} Nodo;

class Lista {
	private:
		Nodo *raiz = NULL;
		Nodo *ultimo = NULL;

	public:
		/* constructor*/
		Lista();
        
		/* crea un nuevo nodo, recibe una instancia de la clase Persona. */
		void crear (int numero);
		/* imprime la lista. */
		void imprimir ();
		void completar_lista(Lista *lista, string letra);
		void mezclar_listas(Lista *mezclada, Lista *original);

};
#endif
